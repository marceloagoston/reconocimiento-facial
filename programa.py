from deepface import DeepFace
import cv2
import matplotlib.pyplot as plt

# img1 representa la foto que vendria por ejemplo de la camara de la tablet
img1 = cv2.imread('./fotos/face1.jpeg')
aciertos = []
aciertosTotales = []
listaFotos = [
	# Parametro Joaquin 1
	('Joaquin','./fotos/joa1.jpeg'),
	('Joaquin','./fotos/joa2.jpeg'),
	('Alan','./fotos/alan1.jpeg'),
	('Alan','./fotos/alan2.jpeg'),
	('Alan','./fotos/alan3.jpeg'),
	# Parametro Alan 4
	('Alan','./fotos/alan4.jpeg'),
	('Alan','./fotos/alan5.jpeg'),
	('Alan','./fotos/alan6.jpeg'),
	('Lucas','./fotos/luk1.jpeg'),
	# Parametro Lucas 2
	('Lucas','./fotos/luk2.jpeg'),
	('Lucas','./fotos/luk3.jpeg'),
	('Lucas','./fotos/luk4.jpeg'),
	('Lucas','./fotos/luk5.jpeg'),
	# Parametro Maxi 1
	('Maxi','./fotos/maxi1.jpeg'),
	('Maxi','./fotos/maxi2.jpeg'),
	# Parametro Orly 1
	('Orly','./fotos/face1.jpeg'),
	('Orly','./fotos/face2.jpeg'),
]

for foto in listaFotos:
	# img1 = cv2.imread(listaFotos[0][1])
	img2 = cv2.imread(foto[1])
	# VGG-Face, Facenet, OpenFace, DeepFace, DeepID, Dlib, ArcFace or Ensemble
	result = DeepFace.verify(img1,img2,model_name = 'DeepID')
	if result["verified"]:
		print(foto[0]+ ' presente!!')
	aciertos.append(result["verified"])
	print('*************************************************************')
	print('Aciertos para Orly (face1) con DeepID', aciertos)

	# {
	# personas:{
	# 	id:1,
	# 	nombre:'Alan',
	# 	apellido:'Fiz',
	# 	foto:'./fotos/face2.jpeg'
	# },
	# asistencia:'Presente'
	# }