from deepface import DeepFace
import cv2
import matplotlib.pyplot as plt

# Para usar modelo Ensemble
import lightgbm as lgb
# img1 representa la foto que vendria por ejemplo de la camara de la tablet
# img1 = cv2.imread('./fotos/joa2.jpeg')
# plt.imshow(img1[:,:,::-1])
# plt.show()
aciertos = []

# img2 = cv2.imread('/home/marcelo/{Cognitis}/proyectos/deepface/joa1.jpeg')
# plt.imshow(img2[:,:,::-1])
# # plt.show()

# Lista de fotos (ponele que traidas de una api)
listaFotos = [
	('Joaquin','./fotos/joa1.jpeg'),
	('Joaquin','./fotos/joa2.jpeg'),
	('Alan','./fotos/alan1.jpeg'),
	('Alan','./fotos/alan2.jpeg'),
	('Alan','./fotos/alan3.jpeg'),
	('Alan','./fotos/alan4.jpeg'),
	('Alan','./fotos/alan5.jpeg'),
	('Alan','./fotos/alan6.jpeg'),
	('Lucas','./fotos/luk1.jpeg'),
	('Lucas','./fotos/luk2.jpeg'),
	('Lucas','./fotos/luk3.jpeg'),
	('Lucas','./fotos/luk4.jpeg'),
	('Lucas','./fotos/luk5.jpeg'),
	('Maxi','./fotos/maxi1.jpeg'),
	('Maxi','./fotos/maxi2.jpeg'),
	('Orly','./fotos/face1.jpeg'),
	('Orly','./fotos/face2.jpeg'),
]

# Itero sobre la lista de fotos y comparo una a una con la img1 
for foto in listaFotos:
	img1 = cv2.imread(foto[1])
	for foto2 in listaFotos:
		img2 = cv2.imread(foto2[1])
		# plt.imshow(img2[:,:,::-1])
		# VGG-Face, Facenet, OpenFace, DeepFace, DeepID, Dlib, ArcFace or Ensemble
		result = DeepFace.verify(img1,img2,model_name = 'Ensemble')
		# print('*************************************************************')
		# print('Comparando '+ foto[0] + ' ' + foto[1] + ' con ' + foto2[0] + '(foto: '+foto2[1]+')')
		# print('Coincide', result)
		if result["verified"]:
			print(foto2[0]+ ' presente!!')
		aciertos.append(result["verified"])
		print('*************************************************************')

	# print('Aciertos para '+ foto[0] + ' con la foto: '+ foto[1])
	# print(aciertos)
	aciertosTotales.append(('Aciertos para '+ foto[0] + ' con la foto: '+ foto[1], aciertos)) 

# print('/////////////////--- ACIERTOS TOTALES ---/////////////////')
# print(aciertosTotales) 